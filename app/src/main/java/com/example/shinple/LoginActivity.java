package com.example.shinple;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;




public class LoginActivity extends AppCompatActivity {

    EditText emp_num ;
    String shared = "file";
    CheckBox log_check ;
    Boolean isChecked = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emp_num = (EditText)findViewById(R.id.emp_num);
        log_check = (CheckBox)findViewById(R.id.log_check);
        SharedPreferences sharedPreferences = getSharedPreferences(shared,0);
        String value = sharedPreferences.getString("id","");
        if(value.length()!=0){
            emp_num.setText(value);
            log_check.setChecked(true);
        }

    }

    public void onClick_join(View v){
        Intent joinintent = new Intent(getApplicationContext(),JoinActivity.class); // 원래는 JoinActivity.class
        startActivity(joinintent);
    }

    public void onClick_login(View v){

        EditText idtext = (EditText)findViewById(R.id.emp_num);
        EditText pwtext = (EditText)findViewById(R.id.editText3);
        String [] personalInfo = new String[2];
        personalInfo[0] = idtext.getText().toString();
        personalInfo[1] = pwtext.getText().toString();


        log_check = (CheckBox)findViewById(R.id.log_check);
        if(log_check.isChecked())                         // 로그인 버튼을 눌렀을때 아이디 저장하기가 체크 되어 있다면
            isChecked = true;                            //isChecked 변수를 true로 바꿈 , 기본적으로는 false임


        Intent menu_intent = new Intent(LoginActivity.this, MainMenuActivity.class);
        startActivity(menu_intent);
        finish();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedPreferences = getSharedPreferences(shared,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();         // Editor를 연결해줌 이 Editor로 값을 저장함
        if(isChecked){                                      //로그인시 아이디 저장하기를 선택해서 체크박스 체크가 되었다면
            String value = emp_num.getText().toString()   ;  //editText에 입력한 값을 String형태로 형변환을 시킨뒤 value에 저장
            editor.putString("id",value);             //id 라는 이름으로 value값을 editor에 저장
            editor.commit();                    //save를 완료 해라 즉, editor.putString()을 완료하라는 의미
        }else{
            String value = "";                              // 체크 되어있지 않았다면 빈값을 넣음
            editor.putString("id",value);
            editor.commit();
        }
    }
}
