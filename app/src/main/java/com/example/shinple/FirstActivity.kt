package com.example.Shinple

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.Shinple.Adapter.FirstAdapter
import com.example.Shinple.Item.Sample
import com.example.shinple.R
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)

        var sample1 = Sample("Chapter00", "AndroidStudio")
        var sample2 = Sample("Chapter01", "LinearLayout")
        var sample3 = Sample("Chapter02", "RelativeLayout")
        var sample4 = Sample("Chapter02", "sample4")
        var sample5 = Sample("Chapter02", "sample5")
        var sample6 = Sample("Chapter02", "sample6")
        var sample7 = Sample("Chapter02", "sample7")
        var sample9 = Sample("Chapter02", "sample9")
        var sample10 = Sample("Chapter02", "sample10")
        var sample11 = Sample("Chapter02", "sample11")
        var sample13 = Sample("Chapter02", "sample13")
        var sample14 = Sample("Chapter02", "sample14")
        var sample15 = Sample("Chapter02", "sample15")
        var sample16 = Sample("Chapter02", "sample16")
        var sample17 = Sample("Chapter02", "sample17")
        var sample18 = Sample("Chapter02", "sample18")
        var sample19 = Sample("Chapter02", "sample19")




        var list = listOf(sample1, sample2, sample3,sample4,sample5,sample6,sample7,sample9,sample10,sample11,sample13,sample14,sample15,sample16,sample17,sample18,sample19)

        rv1_first.adapter = FirstAdapter(this, list)
        rv1_first.layoutManager = LinearLayoutManager(this)
    }
}
