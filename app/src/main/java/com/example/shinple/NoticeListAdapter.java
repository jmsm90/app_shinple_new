package com.example.shinple;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
/*Q&A 게시판 Adapter입니다.*/

public class NoticeListAdapter extends BaseAdapter {

    private Context context;
    private List<Notice> noticelist;

    public NoticeListAdapter(Context context, List<Notice> noticelist) {
        this.context = context;
        this.noticelist = noticelist;
    }

    @Override
    public int getCount() {
        return noticelist.size();
    }

    @Override
    public Object getItem(int i) {
        return noticelist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.notice, null);

        TextView noticeText = (TextView) v.findViewById(R.id.notice_text);
        TextView nameText = (TextView) v.findViewById(R.id.nameText);
        TextView dateText = (TextView) v.findViewById(R.id.dateText);

        noticeText.setText(noticelist.get(i).getNotice());
        nameText.setText(noticelist.get(i).getName());
        dateText.setText(noticelist.get(i).getDate());

        v.setTag(noticelist.get(i).getNotice());
        return v;

    }
}
