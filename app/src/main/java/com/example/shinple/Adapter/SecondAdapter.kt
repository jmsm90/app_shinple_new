package com.example.Shinple.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.Shinple.Item.Sample2
import com.example.Shinple.Item.Sample
import com.example.shinple.R
import kotlinx.android.synthetic.main.item.view.*

class SecondAdapter(private val items: List<Sample2>) : RecyclerView.Adapter<SecondAdapter.ViewHolder>() {
    //var items: MutableList<Sample> = mutableListOf(Sample("test1", "abcd"), Sample("test2", "efgh"), Sample("test4", "asdf"))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].let { item ->
            with(holder) {
                tvTitle.text = item.title
            }
        }
    }

    inner class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)) {
        val tvTitle = itemView.tv_title
    }
}