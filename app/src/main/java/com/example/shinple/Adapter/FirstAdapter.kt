package com.example.Shinple.Adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.Shinple.FirstActivity
import com.example.Shinple.Item.Sample
import com.example.Shinple.SecondActivity
import com.example.shinple.R
import kotlinx.android.synthetic.main.item.view.*

class FirstAdapter(val context: Context, private val items: List<Sample>) : RecyclerView.Adapter<FirstAdapter.ViewHolder>() {
    //var items: MutableList<Sample> = mutableListOf(Sample("test1", "abcd"), Sample("test2", "efgh"), Sample("test4", "asdf"))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].let { item ->
            with(holder) {
                tvTitle.text = item.title
                tvContent.text = item.contents
            }
        }

        holder.itemView.setOnClickListener {
            if(it.tv_title.text == "Chapter01") {
                context.startActivity(Intent(context, SecondActivity::class.java))
            }
        }
    }

    inner class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)) {
        val tvTitle = itemView.tv_title
        val tvContent = itemView.tv_content

    }
}