package com.example.shinple;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.ColorRes;

import androidx.appcompat.app.AppCompatActivity;
import com.example.Shinple.FirstActivity;
import com.example.Shinple.SecondActivity;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;



public class MainMenuActivity extends AppCompatActivity  {
    BoomMenuButton bmb;
    ImageButton cr_btr;
    ImageButton mypage_btr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        settingBoommenu();

        // 나의 강의실 넘어가는 함수 구현
        cr_btr = findViewById(R.id.classroom_btn);
        cr_btr.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View  view) {
                Intent intent = new Intent(MainMenuActivity.this, MyClassRoom.class);
                startActivity(intent);
            }
        });

        //나의 학습 현황 넘어가는 버튼 구현
        ImageButton mypage_btr = findViewById(R.id.learning_status_btn);
        mypage_btr.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainMenuActivity.this, FirstActivity.class);
                startActivity(intent);
            }

        });
        mypage_btr.getBackground().setAlpha(30); // 나의 학습 현황 이미지 배경화면 투명도 조절
    }




    //붐메뉴 버튼 셋팅 함수
    public void settingBoommenu(){
        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        bmb.setNormalColor(getResources().getColor(R.color.colorPrimaryP));
        //마이 페이지 버튼 구현
        TextOutsideCircleButton.Builder home_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.mypage);
        home_builder.normalText("마이페이지");
        home_builder.normalColorRes(R.color.colorPrimaryGrayLight);
        home_builder.normalTextColorRes(R.color.colorNull);
        bmb.addBuilder(home_builder);
        //전체 강좌 버튼 구현
        TextOutsideCircleButton.Builder lecture_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.lecture);
        lecture_builder.normalText("전체 강좌");
        lecture_builder.normalColorRes(R.color.colorPrimaryGrayLight);
        lecture_builder.normalTextColorRes(R.color.colorNull);
        bmb.addBuilder(lecture_builder);
        //공지 사항 버튼 구현
        TextOutsideCircleButton.Builder notice_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.notice);
        notice_builder.normalText("공지사항");
        notice_builder.normalColorRes(R.color.colorPrimaryGrayLight);
        notice_builder.normalTextColorRes(R.color.colorNull);
        bmb.addBuilder(notice_builder);
        //강의노트 게시판 버튼 구현
        TextOutsideCircleButton.Builder note_builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.note);
        note_builder.normalText("강의노트 게시판");
        note_builder.normalColorRes(R.color.colorPrimaryGrayLight);
        note_builder.normalTextColorRes(R.color.colorNull);
        bmb.addBuilder(note_builder);
    }



}
