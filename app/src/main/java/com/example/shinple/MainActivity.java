package com.example.shinple;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent loadintent = new Intent(getApplicationContext(),LoadingActivity.class);
        startActivity(loadintent);
        finish();
    }
}