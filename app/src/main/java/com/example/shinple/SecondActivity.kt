package com.example.Shinple

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.Shinple.Adapter.SecondAdapter
import com.example.Shinple.Item.Sample2
import com.example.shinple.R
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        var sample1 = Sample2("Note_20120302")
        var sample2 = Sample2("Note_20150223")
        var sample3 = Sample2("Note_20140321")
        var sample4 = Sample2("Note_20140321")
        var sample5 = Sample2("Note_20140321")
        var sample6 = Sample2("Note_20140321")
        var sample7 = Sample2("Note_20140321")
        var sample8 = Sample2("Note_20140321")
        var sample9 = Sample2("Note_20140321")

        var list = listOf(sample1, sample2, sample3,sample4,sample5,sample6,sample7,sample8,sample9)

        rv1_second.adapter = SecondAdapter(list)
        rv1_second.layoutManager = LinearLayoutManager(this)
    }
}
