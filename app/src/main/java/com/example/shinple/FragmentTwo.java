package com.example.shinple;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class FragmentTwo extends Fragment {
    private ListView notice_listView;
    private NoticeListAdapter madapter;
    private List<Notice> noticeList;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_two, container, false);



/*

        String [] values =
                {"Time at Residence","Under 6 months","6-12 months","1-2 years","2-4 years","4-8 years","8-15 years","Over 15 years",};
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner.setAdapter(adapter);
*/

        List<String> categories = new ArrayList<String>();

        //String [] values =
          //      {"Time at Residence","Under 6 months","6-12 months","1-2 years","2-4 years","4-8 years","8-15 years","Over 15 years",};
        categories.add("전체");
        categories.add("강의");
        categories.add("기간");
        categories.add("고객");
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item,categories);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);

        /*
        AwesomeSpinner my_spinner = (AwesomeSpinner) view.findViewById(R.id.my_spinner);

        categories.add("Automobile");
        categories.add("Ariplane");

        ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, categories); //fragment로 했기때문에 view와 getActivity를 붙여줘야 레이아웃 받아올 수 있습니다.

        my_spinner.setAdapter(categoriesAdapter);
        my_spinner.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {

            @Override
            public void onItemSelected(int position, String itemAtPosition) {

            }
        });
        */
        /*
        notice_listView = (ListView) view.findViewById(R.id.noticeListView);
        noticeList = new ArrayList<Notice>();
        noticeList.add(new Notice("공지사항", "관리자", "2019-08-22"));
        noticeList.add(new Notice("공지사항", "관리자", "2019-08-22"));
        noticeList.add(new Notice("공지사항", "관리자", "2019-08-22"));
        noticeList.add(new Notice("공지사항", "관리자", "2019-08-22"));
        noticeList.add(new Notice("공지사항", "관리자", "2019-08-22"));
        noticeList.add(new Notice("공지사항", "관리자", "2019-08-22"));

        madapter = new NoticeListAdapter(this.getActivity().getApplicationContext(), noticeList);
        notice_listView.setAdapter(madapter);
        */
        //기본 Adapter로 view를 return 하기 때문에 Q&A 어댑터와 충돌이 날 수도 있습니다.
        //onViewCreated를 만들어주어서 따로 동작하게. Created 다음에 바로 보여주는 것이기 때문에 충돌방지할 수 있는 것 같습니다
    return view;










    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notice_listView = (ListView) view.findViewById(R.id.noticeListView);
        noticeList = new ArrayList<Notice>();
        noticeList.add(new Notice("계정의 비밀번호를 잊어버렸습니다..", "[이용문의]", "2019-08-22"));
        noticeList.add(new Notice("나의 질문 내용", "[강의]", "2019-08-22"));
        noticeList.add(new Notice("나의 질문 내용", "[강의]", "2019-08-22"));
        noticeList.add(new Notice("나의 질문 내용", "[강의]", "2019-08-22"));
        noticeList.add(new Notice("나의 질문 내용", "[강의]", "2019-08-22"));
        noticeList.add(new Notice("나의 질문 내용", "[강의]", "2019-08-22"));

        madapter = new NoticeListAdapter(this.getActivity().getApplicationContext(), noticeList);
        notice_listView.setAdapter(madapter);

    }
}