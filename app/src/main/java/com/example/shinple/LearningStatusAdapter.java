package com.example.shinple;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
/*강의실 게시판 Adapter입니다.*/

public class LearningStatusAdapter extends BaseAdapter {

    private Context context;
    private List<Learning> learninglist;

    public LearningStatusAdapter(Context context, List<Learning> learninglist) {
        this.context = context;
        this.learninglist = learninglist;
    }

    @Override
    public int getCount() {
        return learninglist.size();
    }

    @Override
    public Object getItem(int i) {
        return learninglist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.learning_status, null);

        TextView learning_category =(TextView)v.findViewById(R.id.learning_category);
        TextView lecture_name = (TextView) v.findViewById(R.id.lecture_name);
        TextView learning_date = (TextView) v.findViewById(R.id.learning_date);
        TextView date_num = (TextView) v.findViewById(R.id.date_num);
        TextView progress_rate = (TextView) v.findViewById(R.id.progress_rate);
        TextView progress_my = (TextView) v.findViewById(R.id.progress_my);
        TextView learn_or_not = (TextView) v.findViewById(R.id.learn_or_not);
        TextView learning_check = (TextView) v.findViewById(R.id.learning_check);




        learning_category.setText(learninglist.get(i).getLearning_category());
        lecture_name.setText(learninglist.get(i).getLecture_name());
        learning_date.setText(learninglist.get(i).getLearning_date());
        date_num.setText(learninglist.get(i).getDate_num());
        progress_rate.setText(learninglist.get(i).getProgress_rate());
        progress_my.setText(learninglist.get(i).getProgress_my());
        learn_or_not.setText(learninglist.get(i).getLearn_or_not());
        learning_check.setText(learninglist.get(i).getLearning_check());

        v.setTag(learninglist.get(i).getLearning_category());
        return v;

    }
}
