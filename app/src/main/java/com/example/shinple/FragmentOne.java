package com.example.shinple;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class FragmentOne extends Fragment {
    private ListView learningListView;
    private LearningStatusAdapter ladapter;
    private List<Learning> learningList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_one, container, false);
        learningListView = (ListView) view.findViewById(R.id.learningView);
        learningList = new ArrayList<Learning>();


        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));
        learningList.add(new Learning("[강의]", "안드로이드 개발", "수강일", "2019-08-22~2019-08-31", "진도율",
                "87%", "학습 중?", "수강완료"));



        ladapter = new LearningStatusAdapter(this.getActivity().getApplicationContext(), learningList);
        learningListView.setAdapter(ladapter);
        return view;



    }

}


