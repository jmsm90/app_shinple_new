package com.example.shinple;

public class Learning {
    private String learning_category;
    private String lecture_name;
    private String learning_date;
    private String date_num;
    private String progress_rate;
    private String progress_my;
    private String learn_or_not;
    private String learning_check;

    public Learning(String learning_category, String lecture_name, String learning_date, String date_num, String progress_rate, String progress_my, String learn_or_not, String learning_check) {
        this.learning_category = learning_category;
        this.lecture_name = lecture_name;
        this.learning_date = learning_date;
        this.date_num = date_num;
        this.progress_rate = progress_rate;
        this.progress_my = progress_my;
        this.learn_or_not = learn_or_not;
        this.learning_check = learning_check;
    }

    public String getLearning_category() {
        return learning_category;
    }

    public void setLearning_category(String learning_category) {
        this.learning_category = learning_category;
    }

    public String getLecture_name() {
        return lecture_name;
    }

    public void setLecture_name(String lecture_name) {
        this.lecture_name = lecture_name;
    }

    public String getLearning_date() {
        return learning_date;
    }

    public void setLearning_date(String learning_date) {
        this.learning_date = learning_date;
    }

    public String getDate_num() {
        return date_num;
    }

    public void setDate_num(String date_num) {
        this.date_num = date_num;
    }

    public String getProgress_rate() {
        return progress_rate;
    }

    public void setProgress_rate(String progress_rate) {
        this.progress_rate = progress_rate;
    }

    public String getProgress_my() {
        return progress_my;
    }

    public void setProgress_my(String progress_my) {
        this.progress_my = progress_my;
    }

    public String getLearn_or_not() {
        return learn_or_not;
    }

    public void setLearn_or_not(String learn_or_not) {
        this.learn_or_not = learn_or_not;
    }

    public String getLearning_check() {
        return learning_check;
    }

    public void setLearning_check(String learning_check) {
        this.learning_check = learning_check;
    }
}
