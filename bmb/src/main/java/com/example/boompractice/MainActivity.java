package com.example.boompractice
        ;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

public class MainActivity extends AppCompatActivity {
    BoomMenuButton bmb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bmb = (BoomMenuButton) findViewById(R.id.bmb);

        for(int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i ++)
        {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder().normalImageRes(R.drawable.bee)
                    .normalText("Menu" + (i+1));
                    bmb.addBuilder(builder);
        }
    }
}
